﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HallwayEvent : MonoBehaviour
{

    public GameObject playerCamera;
    public GameObject hallwayCamera;
    public GameObject Alien;
    GameObject player;
    public float timeToSwitchBack = 5.0f;
    public float alienTimer = 1.5f;
    bool HallwayOn = false;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            playerCamera.SetActive(false);
            hallwayCamera.SetActive(true);

            Invoke("AlienActive", alienTimer);

            player.SetActive(false);
            GetComponent<BoxCollider>().enabled = false;
            Invoke("SwitchBack", timeToSwitchBack);
        }
    }

    void AlienActive()
    {
        Alien.SetActive(true);
    }

    void SwitchBack()
    {
        Debug.Log("Camera Back");
        player.SetActive(true);
        playerCamera.SetActive(true);
        hallwayCamera.SetActive(false);

        player.transform.eulerAngles = new Vector3(0, 175, 0);
        player.transform.position = new Vector3(-134, 41, 7);
        
    }
}
